/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode removeNodes(ListNode head) {
        if (head == null) return null;
        
        // Reverse the linked list
        head = reverseList(head);
        
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        
        ListNode current = head;
        ListNode prev = dummy;
        Stack<Integer> stack = new Stack<>();
        
        while (current != null) {
            while (!stack.isEmpty() && stack.peek() <= current.val) {
                stack.pop(); // Pop nodes with values less than or equal to current node's value
            }
            stack.push(current.val);
            
            if (stack.size() > 1) {
                prev.next = current.next; // Remove the current node
            } else {
                prev = current;
            }
            
            current = current.next;
        }
        
        // Reverse the list back to its original order
        return reverseList(dummy.next);
    }
    
    private ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        
        while (current != null) {
            ListNode nextNode = current.next;
            current.next = prev;
            prev = current;
            current = nextNode;
        }
        
        return prev;
    }
}
